import { NestFactory } from '@nestjs/core';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { AppModule } from './modules/app/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('domain-registry')
    .setDescription('the domain API ')
    .setVersion('1.0')
    .addServer('/domains')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  const customOptions: SwaggerCustomOptions = {
    swaggerOptions: {
      persistAuthorization: true,
    },
    customSiteTitle: 'Domain API Docs',
  };

  SwaggerModule.setup('swagger', app, document, customOptions);

  await app.listen(3003);
}
bootstrap();
