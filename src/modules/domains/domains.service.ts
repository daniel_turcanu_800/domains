import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm/dist/common/typeorm.decorators';
import { Repository } from 'typeorm/repository/Repository';
import { DescriptionsService } from '../descriptions/descriptions.service';
import { DomainUpdate } from './domains.types';
import { CreateDomainDto } from './dto/create-domain.dto';
import { Domain } from './entities/domain.entity';

@Injectable()
export class DomainsService {
  constructor(
    @InjectRepository(Domain)
    private readonly domainRepository: Repository<Domain>,
    private readonly descriptionsService: DescriptionsService,
  ) {}

  async create(createDomainDto: CreateDomainDto) {
    if (createDomainDto.description) {
      await this.descriptionsService.create(createDomainDto.description);
    }
    const domain = {
      domain: createDomainDto.domain,
      source: createDomainDto?.source,
    };

    const domainCreated = this.domainRepository.create({
      ...domain,
    });
    return await this.domainRepository.save(domainCreated);
  }

  async findAll(): Promise<Domain[]> {
    const Domain = await this.domainRepository.find();
    if (!Domain) {
      throw new HttpException('Domain not found', HttpStatus.NOT_FOUND);
    }
    return Domain;
  }

  async findOne(id: Domain['id']): Promise<Domain | undefined> {
    const Domain = await this.domainRepository.findOne({ id });
    if (!Domain) {
      throw new HttpException('Domain not found', HttpStatus.NOT_FOUND);
    }
    return Domain;
  }

  async update(id: Domain['id'], updateDomain: DomainUpdate) {
    await this.domainRepository.update({ id }, { ...updateDomain });
    return new HttpException('Update', HttpStatus.OK);
  }

  async remove(id: Domain['id']) {
    const checkIfexistDescription =
      await this.descriptionsService.findByDomainId(id);
    if (checkIfexistDescription) {
      checkIfexistDescription.map((description) => {
        this.descriptionsService.remove(description.id);
      });
    }

    return await this.domainRepository.delete({ id });
  }

  async getDomainsByDomainId(domainId: Domain['id']): Promise<Domain[]> {
    return this.domainRepository.find({ where: { domainId } });
  }
}
