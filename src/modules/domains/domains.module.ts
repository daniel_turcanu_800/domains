import { Module } from '@nestjs/common';
import { DomainsService } from './domains.service';
import { DomainsController } from './domains.controller';
import { DescriptionsModule } from '../descriptions/descriptions.module';
import { Domain } from './entities/domain.entity';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';

@Module({
  imports: [TypeOrmModule.forFeature([Domain]), DescriptionsModule],
  controllers: [DomainsController],
  providers: [DomainsService],
})
export class DomainsModule {}
