import { IsString } from 'class-validator';
import { CreateDescriptionDto } from '../../../modules/descriptions/dto/create-description.dto';

export class CreateDomainDto {
  @IsString()
  domain!: string;

  @IsString()
  source?: string;

  description?: CreateDescriptionDto;
}
