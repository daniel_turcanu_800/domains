import { ApiHideProperty } from '@nestjs/swagger';
import { Description } from '../../../modules/descriptions/entities/description.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('domains')
export class Domain {
  @ApiHideProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: false, unique: true })
  domain: string;

  @Column({ type: 'varchar', nullable: true })
  surce?: string;

  // @OneToOne(() => Description, (description) => description.domainId)
  // user: Description;

  @ApiHideProperty()
  @OneToMany(
    () => Description,
    (domainDescriptions) => domainDescriptions.domain,
  )
  descriptions?: Description[];

  @CreateDateColumn({ nullable: false })
  createdAt?: Date;

  @UpdateDateColumn({ nullable: false })
  updatedAt?: Date;
}
