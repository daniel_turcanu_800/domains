// export type DomainGetAll = {
//   name?: Domain['name'];
// };

import { Domain } from './entities/domain.entity';

export type DomainCreate = Omit<Domain, 'id' | 'createdAt' | 'updatedAt'>;
export type DomainUpdate = Partial<DomainCreate>;
