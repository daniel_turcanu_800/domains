import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  ValidationPipe,
} from '@nestjs/common';
import { DomainsService } from './domains.service';
import { CreateDomainDto } from './dto/create-domain.dto';
import { UpdateDomainDto } from './dto/update-domain.dto';
import { Domain } from './entities/domain.entity';

@Controller('domains')
export class DomainsController {
  constructor(private readonly domainsService: DomainsService) {}

  @Post()
  create(@Body(ValidationPipe) createDomainDto: CreateDomainDto) {
    return this.domainsService.create(createDomainDto);
  }

  @Get()
  findAll() {
    return this.domainsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: Domain['id']) {
    return this.domainsService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: Domain['id'],
    @Body(ValidationPipe) updateDomainDto: UpdateDomainDto,
  ) {
    return this.domainsService.update(id, updateDomainDto);
  }

  @Delete(':id')
  remove(@Param('id') id: Domain['id']) {
    return this.domainsService.remove(id);
  }
}
