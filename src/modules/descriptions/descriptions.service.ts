import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm/dist/common/typeorm.decorators';
import { Repository } from 'typeorm/repository/Repository';
import { Domain } from '../domains/entities/domain.entity';
import { DescriptionCreate, DescriptionUpdate } from './descriptions.types';
import { Description } from './entities/description.entity';

@Injectable()
export class DescriptionsService {
  constructor(
    @InjectRepository(Description)
    private readonly descriptionRepository: Repository<Description>,
  ) {}

  async create(createDescription: DescriptionCreate) {
    const description = this.descriptionRepository.create({
      ...createDescription,
    });
    return await this.descriptionRepository.save(description);
  }

  async findAll(): Promise<Description[]> {
    const description = await this.descriptionRepository.find();
    if (!description) {
      throw new HttpException('description not found', HttpStatus.NOT_FOUND);
    }
    return description;
  }

  async findByDomainId(domainId: Domain['id']): Promise<Description[]> {
    const description = await this.descriptionRepository.find({ domainId });
    return description;
  }

  async findOne(id: Description['id']): Promise<Description | undefined> {
    const description = await this.descriptionRepository.findOne({ id });
    if (!description) {
      throw new HttpException('description not found', HttpStatus.NOT_FOUND);
    }
    return description;
  }

  async update(id: Description['id'], updateDescription: DescriptionUpdate) {
    await this.descriptionRepository.update({ id }, { ...updateDescription });
    return new HttpException('Update', HttpStatus.OK);
  }

  async remove(id: Description['id']) {
    return await this.descriptionRepository.delete({ id });
  }
}
