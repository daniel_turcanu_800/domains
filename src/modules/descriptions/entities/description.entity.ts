import { ApiHideProperty } from '@nestjs/swagger';
import { Domain } from '../../../modules/domains/entities/domain.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { DescriptionStatus } from '../descriptions.types';

@Entity('descriptions')
export class Description {
  @ApiHideProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: false })
  description: string;

  @Column({
    type: 'enum',
    enum: DescriptionStatus,
    default: DescriptionStatus.PUBLIC,
    nullable: false,
  })
  status: DescriptionStatus;

  //   @OneToOne(() => Domain)
  //   @JoinColumn()
  //   domainId: Domain;

  @Column()
  domainId: number;
  @ApiHideProperty()
  @ManyToOne(() => Domain, (domain) => domain.descriptions)
  @JoinColumn({ name: 'domainId' })
  domain?: Domain;

  @CreateDateColumn({ nullable: false })
  createdAt?: Date;

  @UpdateDateColumn({ nullable: false })
  updatedAt?: Date;
}
