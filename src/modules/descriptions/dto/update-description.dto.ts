import { CreateDescriptionDto } from './create-description.dto';

export class UpdateDescriptionDto extends CreateDescriptionDto {}
