import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsInt, IsOptional, IsString } from 'class-validator';
import { Domain } from '../../../modules/domains/entities/domain.entity';
import { DescriptionStatus } from '../descriptions.types';

export class CreateDescriptionDto {
  @IsInt()
  domainId!: Domain['id'];

  @IsString()
  description!: string;

  @ApiProperty({ enum: DescriptionStatus })
  @IsOptional()
  @IsEnum(DescriptionStatus)
  status!: DescriptionStatus;
}
