import { Description } from './entities/description.entity';

export const DescriptionStatus = {
  HIDDEN: 'hidden',
  PUBLIC: 'public',
  DRAFT: 'draft',
} as const;
export type DescriptionStatus =
  typeof DescriptionStatus[keyof typeof DescriptionStatus];

// export type DescriptionGetAll = {
//   name?: Description['name'];
// };

export type DescriptionCreate = Omit<
  Description,
  'id' | 'createdAt' | 'updatedAt'
>;
export type DescriptionUpdate = Partial<DescriptionCreate>;
