import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  ValidationPipe,
} from '@nestjs/common';
import { DescriptionsService } from './descriptions.service';
import { CreateDescriptionDto } from './dto/create-description.dto';
import { UpdateDescriptionDto } from './dto/update-description.dto';
import { Description } from './entities/description.entity';

@Controller('descriptions')
export class DescriptionsController {
  constructor(private readonly descriptionsService: DescriptionsService) {}

  @Post()
  create(@Body(ValidationPipe) createDescriptionDto: CreateDescriptionDto) {
    return this.descriptionsService.create(createDescriptionDto);
  }

  @Get()
  findAll() {
    return this.descriptionsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: Description['id']) {
    return this.descriptionsService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: Description['id'],
    @Body(ValidationPipe) updateDescriptionDto: UpdateDescriptionDto,
  ) {
    return this.descriptionsService.update(id, updateDescriptionDto);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: Description['id']) {
    return this.descriptionsService.remove(id);
  }
}
