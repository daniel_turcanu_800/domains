import { Module } from '@nestjs/common';
import { DomainsModule } from '../domains/domains.module';
import { DescriptionsModule } from '../descriptions/descriptions.module';
import config from '../../config/config';
import { environmentSchema } from './environment.validation-schema';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
      validationSchema: environmentSchema,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => config.get('database'),
      inject: [ConfigService],
    }),
    DomainsModule,
    DescriptionsModule,
  ],
})
export class AppModule {}
