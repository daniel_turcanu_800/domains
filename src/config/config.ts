export default () => ({
  database: {
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    port: +process.env.POSTGRES_PORT,
    database: process.env.POSTGRES_DB,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    ssl: +process.env.POSTGRES_SSL ? { rejectUnauthorized: false } : false,
    synchronize: process.env.NODE_ENV === 'local',
    logging: false,
    migrationsRun: true,
    entities: ['dist/**/*.entity.{js,ts}'],
    migrations: ['dist/**/migrations/*.{js,ts}'],
    subscribers: ['dist/**/subscriber/**/*.{js,ts}'],
    cli: {
      migrationsDir: 'src/migrations',
      subscribersDir: 'src/subscribers',
    },
    extra: {
      connectionLimit: 50,
    },
  },
});
